﻿using System;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            bool run = true;
            while (run)
            {
                int[] arr = { 1, 9, 7, 1, 2, 5, 2, 5, 6, 8 };

                Console.WriteLine("Select from the following sorting methods:");
                Console.WriteLine("1. Bubble Sort");
                Console.WriteLine("2. Bubble Sort Recursive");
                Console.WriteLine("3. Selection Sort");
                Console.WriteLine("4. Insertion Sort");
                Console.WriteLine("5. Insertion Sort Recursive");
                Console.WriteLine("6. Merge Sort");
                Console.WriteLine("");
                Console.WriteLine("0. Exit");
                Console.WriteLine("");
                int sortSelection = Convert.ToInt32(Console.ReadLine());
                if(sortSelection != 0)
                {
                    Console.WriteLine("");
                    PrintArr(arr, "Start: ");
                    switch (sortSelection)
                    {
                        case 1:
                            Console.WriteLine("Bubble Sort");
                            BubbleSort(arr);
                            break;
                        case 2:
                            Console.WriteLine("Bubble Sort Recursive");
                            BubbleSortRecursive(arr, arr.Length);
                            break;
                        case 3:
                            Console.WriteLine("Selection Sort");
                            SelectionSort(arr);
                            break;
                        case 4:
                            Console.WriteLine("Insertion Sort");
                            InsertionSort(arr);
                            break;
                        case 5:
                            Console.WriteLine("Insertion Sort Recursive");
                            InsertionSortRecursive(arr, arr.Length);
                            break;
                        case 6:
                            MergeSort(arr, 0, arr.Length - 1);
                            break;
                        default:
                            run = false;
                            return;
                    }
                    PrintArr(arr, "Output: ");
                    Console.WriteLine("");
                    Console.WriteLine("---------------");
                    Console.WriteLine("");
                }
                else
                {
                    run = false;
                }
               
            }
            

        }

        static void PrintArr(int []arr, string message)
        {
            Console.WriteLine(message);
            for(int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
                
            }
            Console.WriteLine();
        }

        //bubble sort: avg: O(n^2)
        //"Bubble" the biggest number to the end.
        //Explination: First loop sends the biggest number to the end of the array, next loop puts biggest number in the second to last spot and so on.
        static void BubbleSort(int[] arr)
        {
            //loop through full array - 1
            for (int i = 0; i < arr.Length - 1; i++)
            {
                //loop through array length minus the number of times you have already bubbled minus the last element(Arr out of bounds exception)
                for (int j = 0; j < arr.Length - i - 1; j++)
                {
                    //swap the numbers if the item to the left if bigger.
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }

            return;
        }

        //bubble sort recursive: avg: O(n^2)
        //"Bubble" the biggest number to the end.
        //Explination: Recursive is the same as the non recursive just a for loop is elimiated by the call of itself. The call reduces the length of the looped array by 1 until we get to the base case length of 1. The 1 indicates the whole array has been sorted.
        static void BubbleSortRecursive(int[] arr, int n)
        {
            //base case
            if (n == 1)
            {
                return;
            }
            else
            {
                //loop through array to the given length minus the last element(Arr out of bounds exception)
                for (int i = 0; i < n - 1; i++)
                {
                    //swap the numbers if the item to the left if bigger.
                    if (arr[i] > arr[i + 1])
                    {
                        int temp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = temp;
                    }
                }

                //call yourself again, subtract the position you just sorted.
                BubbleSortRecursive(arr, n - 1);
            }
        }

        //Selection Sort: avg: O(n^2)
        //Find the minimum number, swap that number with the boundery position(i)
        //Explination: First loop: loop through entire arr, set current minimum to the current position. Second loop: loop through i + 1 till the end and find the minimum, swap minimum with i position.
        static void SelectionSort(int[] arr)
        {

            for (int i = 0; i < arr.Length - 1; i++)
            {
                //set starting minimum to current value
                int min_index = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    //check if j is the new minmum if so set new index to j
                    if(arr[min_index] > arr[j])
                    {
                        min_index = j;
                    }
                }

                //swap min with i
                int temp = arr[min_index];
                arr[min_index] = arr[i];
                arr[i] = temp;
            }

            return;
        }

        //Insertion Sort: avg O(2n)
        //Move left to right, if key number is less then number to its left shift bigger number over 1 position and continue iterating through array till key value is bigger then the number to its left, insert number to porper position
        //Explination: for loop: set key value you are inserting, set starting value j. while loop: if key value is less then the comparing value shift comparing value to the right and compare key to the next value untill the comparing value is smaller. insert key value to the new postion.
        static void InsertionSort(int[] arr)
        {
            //loop through full array starting with position 2.
            for (int i = 1; i < arr.Length; ++i)
            {
                //set key value (current i pointer)
                int key = arr[i];
                //starting comparitiave value
                int j = i - 1;

                //while j > 0 and key is less then caparing value shift comparing value to the right
                while (j >= 0 && arr[j] > key)
                {
                    //shift arr value to the right
                    arr[j + 1] = arr[j];
                    j = j - 1;
                }
                //set key to the correct position
                arr[j + 1] = key;
            }


            return;
        }

        //Insertion Sort Recursive: avg O(2n)
        //Base case is length of arr is 1
        //Explination: check base case, if length is greater then 1 start recursion by calling on the next case. Work right to left. Each recursion level is responsible for their arr.Length - 1 as their key, find where that key is supose to be place and continue down the recursion tree.
        //recursion gets called all n times, then cascades up the tree similar to non recursion version.
        static void InsertionSortRecursive(int[] arr, int n)
        {
            //base case
            if(n <= 1)
            {
                return;
            }
            //start recursion
            InsertionSortRecursive(arr, n - 1);

            //set key value
            int last = arr[n - 1];
            //start j to the left of your key value
            int j = n - 2;

            //loop through till j = -1 or the key value is greater than j value (looping right to left on arr)
            while (j >= 0 && arr[j] > last)
            {
                //move j value to the right
                arr[j + 1] = arr[j];
                j--;
            }
            //place key value in correct spot.
            arr[j + 1] = last;
        }


        static void MergeSort(int[] arr, int left, int right)
        {
            if (left < right)
            {
                int mid = (left + right) / 2;
                MergeSort(arr, left, mid);
                MergeSort(arr, mid + 1, right);

                Merge(arr, left, mid, right);
            }
            return;
        }

        static void Merge(int[] arr, int left, int mid, int right)
        {
            

            int[] leftArr = new int[mid - left + 1];
            int[] rightArr = new int[right - mid];

            Array.Copy(arr, left, leftArr, 0, mid - left + 1);
            Array.Copy(arr, mid + 1, rightArr, 0, right - mid);

            PrintArr(leftArr, "Left Arr: ");
            PrintArr(rightArr, "Right Arr: ");

            int i = 0;
            int j = 0;

            Console.WriteLine("Left: " + left + " Right: " + right);

            for (int k = left; k < right + 1; k++)
            {
                Console.WriteLine("loop" + k);
                if (i == leftArr.Length)
                {
                    Console.WriteLine(i + "==" + leftArr.Length);
                    arr[k] = rightArr[j];
                    j++;
                }
                else if (j == rightArr.Length)
                {
                    Console.WriteLine(j + "==" + rightArr.Length);
                    arr[k] = leftArr[i];
                    i++;
                }
                else if (leftArr[i] <= rightArr[j])
                {
                    Console.WriteLine(leftArr[i ]+ "<=" + rightArr[j]);
                    arr[k] = leftArr[i];
                    i++;
                }
                else
                {
                    Console.WriteLine(leftArr[i] + ">" + rightArr[j]);
                    arr[k] = rightArr[j];
                    j++;
                }
            }
            return;
        }
    }
}
